export module Artist {

  export interface Model {
    albums: string[];
    singerOf: string[];
    writerOf: any[];
    composerOf: any[];
    name: string;
    avatar: string;
    bio: string;
    updatedAt: Date;
    id: string;
    sex: string;
    updated_at?: Date;
    created_at?: Date;
    music: string[];
  }

}

