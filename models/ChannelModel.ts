export module Channel {

  export interface Model {
    musics: string[];
    genres: string[];
    name: string;
    coverImage: string;
    bgColor: string;
    updatedAt: Date;
    id: string;
  }

}

