export module Music {
    export class Model {
        likes: number;
        views: number;
        genres: string[];
        singers: any[];
        writers: any[];
        composers: any[];
        name: string;
        releaseDate?: string;
        coverImage: string;
        audioLink: string;
        updatedAt?: string;
        channels?: string[];
        color?: string;
        releasedata?: string;
        updated_at?: string;
        artists?: string[];
        genre?: string[];
        album?: string;
        id: string;
        created_at?: string;
        slider?: string[];

        constructor() { 
            this.likes = 0;
            this.views = 0;
            this.genres= [];
            this.singers = []; 
            this.writers = [];
            this.composers = []
            this.name= "";
            this.coverImage = "~assets/defaultcover.jpg";
            this.audioLink = "";
            this.id = "-1";
        }
    }
}