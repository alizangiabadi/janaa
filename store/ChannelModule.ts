import {Module, VuexModule, Mutation, Action} from 'vuex-module-decorators'
import {Api} from "~/services/ChannelService";
import getAllChannels = Api.getAllChannels;
import { Channel } from '~/models/ChannelModel';
import ChannelModel = Channel.Model;

@Module({
  stateFactory: true,
  namespaced: true
})
export default class ChannelModule extends VuexModule {
  channels: ChannelModel[] = [];

  @Mutation
  setChannels(channels: ChannelModel[]) {
    this.channels = channels;
  }

  get channelList() {
    return this.channels;
  }

  @Action({rawError: true})
  async getChannels() {
    const tmp = await getAllChannels();
    this.context.commit('setChannels',tmp);
   // this.channels = tmp;
  }
}
