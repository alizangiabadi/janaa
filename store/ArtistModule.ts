import {Module, VuexModule, Mutation, Action} from 'vuex-module-decorators'
import {Api} from "~/services/ArtistService";
import getAllArtists = Api.getAllArtists;
import { Artist } from '~/models/ArtistModel';
import ArtistModel = Artist.Model;

@Module({
  stateFactory: true,
  namespaced: true
})
export default class ArtistModule extends VuexModule {
  artists: ArtistModel[] = [];

  @Mutation
  setArtists(artists: ArtistModel[]) {
    this.artists = artists;
  }

  get artistList() {
    return this.artists;
  }

  @Action({rawError: true})
  async getArtists() {
    const tmp = await getAllArtists();
    this.context.commit('setArtists',tmp);
  }
}
