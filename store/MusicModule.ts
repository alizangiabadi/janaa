import {Module, VuexModule, Mutation, Action} from 'vuex-module-decorators'
import {Api} from "~/services/MusicService";
import getAllMusics = Api.getAllMusics;
import { Music } from '~/models/MusicModel';
import MusicModel = Music.Model;

@Module({
  stateFactory: true,
  namespaced: true
})
export default class MusicModule extends VuexModule {
  musics: MusicModel[] = [];

  @Mutation
  setMusics(musics: MusicModel[]) {
    this.musics = musics;
  }

  get muiscList() {
    return this.musics;
  }

  @Action({rawError: true})
  async getMusics() {
    const tmp = await getAllMusics();
    this.context.commit('setMusics',tmp);
  }
}
