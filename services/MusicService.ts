import {$axios} from '~/utils/api'
import { Music } from '~/models/MusicModel';


export module Api {
  export async function getAllMusics() {
    return await $axios.$get<Music.Model[]>('/musics');
  }
}
