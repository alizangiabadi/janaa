import {$axios} from '~/utils/api'
import { Artist } from "~/models/ArtistModel";

export module Api {
  export async function getAllArtists() {
    return await $axios.$get<Artist.Model[]>('/artists');
  }
}
