import {$axios} from '~/utils/api'
import { Channel } from "~/models/ChannelModel";

export module Api {
  export async function getAllChannels() {
    return await $axios.$get<Channel.Model[]>('/channels');
  }
}
